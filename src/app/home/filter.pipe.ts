import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPosts = [];
    for(const empleados of value){
      if(empleados.nombre.indexOf(arg) > -1){
         resultPosts.push(empleados);
      };
    };
    return resultPosts;
  }

}
